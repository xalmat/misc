#!/bin/bash
for YY in {2000..2020}

do
    for MM in {01..12}
    do 
        for DD in {01..31}
        do
            FILECOUNT=$(ls *$YY$MM$DD*.* | wc -l)
            FILECOUNT2=$(ls *$YY-$MM-$DD*.* | wc -l)

            if [ $FILECOUNT -ge 1 ];
            then
                sortdate=$(date +%Y%m%d-%H%M%S)
               mkdir ./$YY-$MM-$DD-sorted-$sortdate
               mv ./*$YY$MM$DD*.* ./$YY-$MM-$DD-sorted-$sortdate/
            fi

            if [ $FILECOUNT2 -ge 1 ];
            then
                sortdate=$(date +%Y%m%d-%H%M%S)
                mkdir ./$YY-$MM-$DD-sorted-$sortdate
                mv ./*$YY-$MM-$DD*.* ./$YY-$MM-$DD-sorted-$sortdate/
            fi

        done
    
    done
    
done