Unless otherwise noted, all files are released under the Unlicense License. See [LICENSE](LICENSE) for details.

[quick-picture-sort.sh](quick-picture-sort.sh) - Simple script that takes a folder containing files in the filename format "*YYYYMMDD*.*" or "*YYYY-MM-DD*.*" and automatically sorts them into respective subfolders by date. Great for quickly sorting files taken off of a phone's SD card and quickly sorting them. Each subfolder is timestamped to help with dealing with duplicate filenames.
